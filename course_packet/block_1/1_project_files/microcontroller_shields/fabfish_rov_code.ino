/* 
Notes:
Arduino pins 3,5,6,9,10,and 11 are used as they are all PWM (pulse-width modulation) pins.
Therefore, we can use the analogWrite command to set PWM duty cycle to control motor speed.

*/
                           // DIVE/SURFACE: ORANGE PAIR
int motorPinSURF = 5;      // surf (surface- motor runs forward)     
int motorPinDIVE = 6;      // dive (dive- motor runs backward)      
              

                           // STARBOARD: GREEN PAIR
int motorPinSF = 3;        // sf (starboard forward)- green
int motorPinSB = 9;        // sb (starboard backward)- lt green


                           // PORT: BLUE PAIR
int motorPinPF = 10;       // pf (port forward)- blue
int motorPinPB = 11;       // pb (port backward)- lt blue

void setup() {
  
Serial.begin(9600);           
Serial.println("motor control");

}


void loop() {


   if  (Serial.available() > 0) {       
    switch(Serial.read()) {  

  
   case 's':           
analogWrite(motorPinSURF, 200);   
Serial.println("surface/forward");
   break;  

 case 'd': 
analogWrite(motorPinDIVE, 200);  
Serial.println("dive/backward");
   break; 

 case '6': 
analogWrite(motorPinSF, 200);  
Serial.println("starboard forward");
   break; 

    case '2': 
analogWrite(motorPinSB, 200);  
Serial.println("starboard backward");
   break; 


     case '3': 
analogWrite(motorPinPF, 200);  
Serial.println("port forward");
   break; 

     case '4': 
analogWrite(motorPinPB, 200);  
Serial.println("port backward");
   break; 

 case 'x': 
analogWrite(motorPinSF, 200);  
analogWrite(motorPinPF, 200);  
Serial.println("both forward");
   break; 

case ' ': 
digitalWrite(motorPinDIVE, LOW);  
digitalWrite(motorPinSURF, LOW);
digitalWrite(motorPinSF, LOW);  
digitalWrite(motorPinSB, LOW);
digitalWrite(motorPinPF, LOW);  
digitalWrite(motorPinPB, LOW);
Serial.println("stopped");
   break; 

   }
   }
}

