/*

building this code following this tutorial:
https://learn.sparkfun.com/tutorials/connecting-arduino-to-processing
http://faculty.purchase.edu/joseph.mckay/physicalcomputing2015/serial.php

KZ: upload this Arduino code through USB cable, not bluetooth
KZ: "Serial" is on pins 0&1, "Serial1" is on pins 19&18; check here for more info on Arduino Mega serial communicaiton:
 https://www.arduino.cc/en/Reference/Serial
 
In this code, we are using "Serial" for the sensor, and "Serial1" for the motors

 // have to look up how to connect to serial ports 0 and 1 on the mega
 
 */
 



//KZ: reads information coming from Processsing to Arduino--------------------------------
// char val; // incoming data from the serial port  (is this the right type of variable?  do I need to tell it which port to use?)
// int ledPin = 13; // sets up the Arduino pin to digital I/O 13  (think I need analog/ PWM pin)
// boolean ledState = LOW;  //don't think I need this because I am using analog

int valuein; 
int valueout; 
int input_pin_from_processing = 1;    //KZ: note this is the same pin the sensor is connected to right now
int motorpin1 = 13; 

void setup()
{
 //KZ: reads information coming from Processsing to Arduino--------------------------------   
  Serial1.begin(57600);   
  pinMode(motorpin1, OUTPUT);  
    
 //KZ: reads information coming from Arduino to Processing--------------------------------
  Serial.begin(57600);  
}



void loop()
{
  
  valueout = analogRead(input_pin_from_processing)/4; 
  Serial1.write(valueout); 


  if (Serial1.available()){
    valuein = Serial1.read(); 
  }
  if (valuein == 'H'){
    digitalWrite(motorpin1, HIGH) ;
  }
  else if (valuein == 'L'){
    digitalWrite(motorpin1, LOW) ;  
  } 
  delay(10); 
  
  
    
      // send the value of analog input 0:
  Serial.println(analogRead(A0));
  // wait a bit for the analog-to-digital converter
  // to stabilize after the last reading:
  delay(2);
  
 
  
}



